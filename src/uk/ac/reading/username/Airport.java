package uk.ac.reading.username;


public class Airport
{
	private String airportName;
	private String airportIATAFFA;
	private String latitude;
	private String longitude;
	
	/**
	 * @return the airportName
	 */
	public String getAirportName()
	{
		return airportName;
	}
	/**
	 * @param airportName the airportName to set
	 */
	public void setAirportName(String airportName)
	{
		this.airportName = airportName;
	}
	/**
	 * @return the airportIATAFFA
	 */
	public String getAirportIATAFFA()
	{
		return airportIATAFFA;
	}
	/**
	 * @param airportIATAFFA the airportIATAFFA to set
	 */
	public void setAirportIATAFFA(String airportIATAFFA)
	{
		this.airportIATAFFA = airportIATAFFA;
	}
	/**
	 * @return the latitude
	 */
	public String getLatitude()
	{
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude)
	{
		this.latitude = latitude;
	}
	/**
	 * @return the longitude
	 */
	public String getLongitude()
	{
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude)
	{
		this.longitude = longitude;
	}
	
	
	
	
}
