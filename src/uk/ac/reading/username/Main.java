package uk.ac.reading.username;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main 
{	
	public static void main(String[] args) 
	{
		/*User input process*/
		System.out.println("Type in the file path for passenger data(Should be like 'c:/Data/AComp_Passenger_data.csv'): ");
		Scanner scanner = new Scanner(System.in);
		String csvPassengerUser = scanner.nextLine();
		System.out.println("Type in the file path for airport data:(Should be like 'c:/Data/Top30_airports_LatLong.csv')");
		String csvAirportUser = scanner.nextLine();
		
		System.out.println("What job do you want to do?");
		System.out.println("1 . List all airports and their flights");
		System.out.println("2 . List all flights with passenger detail");
		System.out.println("3 . List all flights with the number of passengers");
		int choice = scanner.nextInt();
		

		String subject = "flightsFromAirport";
		
		switch(choice)
		{
			case 1: subject = "flightsFromAirport";
				break;
			case 2: subject = "listFlights";;
				break;
			case 3: subject = "listPassengers";
				break;
			
		}
		
		System.out.println("Type in the output path ( Leave blank to create folder at 'c:/TestFolder/ 'Should be in format 'c:/path/') :");
		String csvOutput = scanner.nextLine();
		System.out.println("Type in the file name for the output (Leave blank to name file 'test.csv' Should be in format 'filename.csv'):");
		String csvFileName = scanner.nextLine();
			

		scanner.close();
		
		String csvPassenger = "G:\\PC\\Desktop\\University Work\\Part 3\\Advanced Computing\\CourseworkAComp_Passenger_data.csv";  // for debugging
		String csvAirport = "G:\\PC\\Desktop\\University Work\\Part 3\\Advanced Computing\\Coursework\\Top30_airports_LatLong.csv"; // for debugging
		
		
		if(csvOutput.isEmpty())
		{
			csvOutput = "c:/TestFolder/";
		}
		if(csvFileName.isEmpty())
		{
			csvFileName = "test.csv";
		}
		
		
		/*Variables for use in the system*/
		LinkedList<Passenger> passengerListInitial = createPassengerList(csvPassengerUser);
		LinkedList<Airport> airportList = createAirportList(csvAirportUser);
		LinkedList<String> knownFlightCode = createKnownFlightCode(airportList);
		Queue<Passenger> passengerError = new LinkedList<Passenger>();
		Queue<Passenger> passengerQueueDuplicate = new LinkedList<Passenger>();
		Queue<Passenger> passengerQueueNon = new LinkedList<Passenger>();
		
		
		/*seperates error from non error*/
		for(int i = 0; i < passengerListInitial.size(); i++)
		{
			Passenger tempPassenger = passengerListInitial.get(i);
			if(checkPassengerRegex(tempPassenger) && knownFlightCode.contains(tempPassenger.getStartIATAFFAcode()) 
					&& knownFlightCode.contains(tempPassenger.getEndIATAFFAcode()))
			{
				passengerQueueDuplicate.add(tempPassenger);
			}
			else
			{
				passengerError.add(tempPassenger);
			}			
		}
		
		passengerQueueNon = removeDuplicates(passengerQueueDuplicate); // remove duplicates from the clean list
		LinkedList<String> knownFlightID = createKnownFlightIDList(new LinkedList<Passenger>(passengerQueueDuplicate)); // creates known flight id from clean list
		passengerError = fixErrors(passengerError, knownFlightID, knownFlightCode); // fixes the passenger errors
		passengerQueueNon.addAll(passengerError); // adds the fixed errors to the clean list
		passengerQueueNon = removeDuplicates(passengerQueueNon); // once again remove any duplicates introduced by the error list
		
		/*---------------------start of map reduce---------------------------------------- */
		
		/* varibles for used in the map shuffle and reduce threads*/
		LinkedList<Passenger> test = new LinkedList<Passenger>(); // initial passenger list should be alot
		test.addAll(passengerQueueNon);
		BlockingQueue<Tuple> sharedList = new LinkedBlockingQueue<Tuple>(); // this is shared
		BlockingQueue<TupleList> tupleList = new LinkedBlockingQueue<TupleList>(); 
		Mapper mapper = new Mapper(test, sharedList, subject); // initialises mapper
		Shuffler shuffler = new Shuffler(sharedList, tupleList); // initialises shuffler
		Reducer reducer = new Reducer(tupleList, subject, airportList, csvOutput, csvFileName);// initialises reducer
		
		mapper.start();
		try
		{
			mapper.join(); // wait for mapper to finish before shuffler start
		} 
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		
		shuffler.start(); 
		
		try
		{
			shuffler.join(); // wait for shuffler to finish before reducer start 
		} 
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		reducer.start(); // start the reducer
	}
	
	
	
	
	
	
	
	
	
	/*
	 * Returns the minimum between three values
	 */
	private static int minimum(int a, int b, int c) 
	 {                            
	        return Math.min(Math.min(a, b), c);                                      
	 }   
	 
	
	
	/**
	 *  Returns the levenshtein distance two strings
	 * @param lhs first string to compare with
	 * @param rhs second string to compare with
	 * @return the distance between the two strings
	 */
	 private static int computeLevenshteinDistance(CharSequence lhs, CharSequence rhs) 
	 {      
	        int[][] distance = new int[lhs.length() + 1][rhs.length() + 1];        
	                                                                                 
	        for (int i = 0; i <= lhs.length(); i++)                                 
	            distance[i][0] = i;                                                  
	        for (int j = 1; j <= rhs.length(); j++)                                 
	            distance[0][j] = j;                                                  
	                                                                                 
	        for (int i = 1; i <= lhs.length(); i++)                                 
	            for (int j = 1; j <= rhs.length(); j++)                             
	                distance[i][j] = minimum(                                        
	                        distance[i - 1][j] + 1,                                  
	                        distance[i][j - 1] + 1,                                  
	                        distance[i - 1][j - 1] + ((lhs.charAt(i - 1) == rhs.charAt(j - 1)) ? 0 : 1));
	                                                                                 
	        return distance[lhs.length()][rhs.length()];                           
	 }  

	 /**
	  *  Fixes the errors of a given list
	  * @param errorList error list to be fixed
	  * @param knownFlightID flight id list which is known to be correct
	  * @param knownFlightCode aiport IATAFFA codes which is know to be correct
	  * @return
	  */
	private static LinkedList<Passenger> fixErrors(Queue<Passenger> errorList, LinkedList<String> knownFlightID, LinkedList<String> knownFlightCode)
	{
		LinkedList<Passenger> noErrorList = new LinkedList<Passenger>();
		// remove all 
		int limit = errorList.size();
		for(int i = 0; i < limit;i++)
		{
			/*gets a passenger from the error lists and create containers for the passenger strins*/
			Passenger tempPassenger = errorList.poll();
			String passengerID = tempPassenger.getPassengerID();
			String flightID = tempPassenger.getFlightID();
			String startIATAFFAcode = tempPassenger.getStartIATAFFAcode();
			String endIATAFFAcode = tempPassenger.getEndIATAFFAcode();
			String departureTime = tempPassenger.getDepartureTime();
			
			
			
			tempPassenger.setPassengerID(fixPassengerID(passengerID)); // fixes the passenger id
			tempPassenger.setFlightID(fixFlightID(flightID, knownFlightID)); // fixes the flight id
			tempPassenger.setStartIATAFFAcode(fixFlightCode(startIATAFFAcode, knownFlightCode)); // fixes the from IATAFFA Code
			tempPassenger.setEndIATAFFAcode(fixFlightCode(endIATAFFAcode, knownFlightCode)); // fixes the to IATAFFA Code
			
			// fix codes
			
			// convert times
			
			if((!fixPassengerID(passengerID).isEmpty()) 
				)
			{
				noErrorList.add(tempPassenger); // adds the passenger to no error list if not empty
			}
		}
		return noErrorList;
	}
	
	/*
	 * fixes the flight code based on levenshete distance given the a code and the list of known codes
	 */
	private static String fixFlightCode(String flightCode, LinkedList<String> knownFlightCode)
	{	
		int levContainer = 0;
		int indexTracker = 0;
		String firstAirport = knownFlightCode.get(0);
		levContainer = computeLevenshteinDistance(flightCode, firstAirport); // pitfall if the numbers are equal 
		if(levContainer != 0)
		{
			for(int i = 1; i < knownFlightCode.size(); i++)
			{				
				int tempLev = computeLevenshteinDistance(flightCode, (knownFlightCode.get(i)));
				if(levContainer> tempLev)
				{
					levContainer = tempLev;
					indexTracker = i;
				}
			}	
		}
		
		
		
		return knownFlightCode.get(indexTracker);
	}
	
	
	/*
	 * fixes the flight id based on levenshte distance given the flight id and known flightid list
	 */
	private static String fixFlightID(String flightID, LinkedList<String> knownFlightID) // works but make a list of known flight id and compare
	{
		int levContainer = 0;
		int indexTracker = 0;
		String firstID = knownFlightID.get(0);
		levContainer = computeLevenshteinDistance(flightID, firstID); // pitfall if the numbers are equal 
		if(levContainer != 0) // shouldnt be 0 or else it is sorted into wrong list
		{
			for(int i = 1; i < knownFlightID.size(); i++)
			{				
				int tempLev = computeLevenshteinDistance(flightID, (knownFlightID.get(i)));
				if(levContainer > tempLev)
				{
					levContainer = tempLev;
					indexTracker = i;
				}		
			}
		}
		return knownFlightID.get(indexTracker); 
	}
	
	/*
	 * fixes the passenger id based on conditions set by the regular expressions
	 */
	private static String fixPassengerID(String passengerID)
	{
		StringBuilder sb = new StringBuilder();
		//PUD 8209 OG 3
		if(passengerID.length() == 10) // if correct length then procede normally 
		{
			for(int i = 0; i < passengerID.length();i++)
			{
				if(i < 3) // check if first three are letters
				{
					if(Character.isLetter(passengerID.charAt(i)))
					{
						
						sb.append(Character.toUpperCase(passengerID.charAt(i))); // add to string if char or num
					}
					else
					{
						sb.append("A"); // replace with A if not character
					}
				}
				else if(i < 7)
				{
					if(Character.isDigit(passengerID.charAt(i)))
					{
						sb.append(passengerID.charAt(i));
					}
					else
					{
						sb.append("0");
					}
				}
				else if(i < 9)
				{
					if(Character.isLetter(passengerID.charAt(i)))
					{
						sb.append(Character.toUpperCase(passengerID.charAt(i)));
					}
					else
					{
						sb.append("A");
					}
				}
				else
				{
					if(Character.isDigit(passengerID.charAt(i)))
					{
						sb.append(passengerID.charAt(i));
					}
					else
					{
						sb.append("0");
					}
				}
							
			}
		}
		else
		{
			for(int i = 0; i < passengerID.length();i++)
			{
				if(Character.isLetterOrDigit(passengerID.charAt(i)))
				{
					sb.append(passengerID.charAt(i));
				}
			}
		}
		
		return sb.toString();
	}
	
	/*
	 * creates a list of known flight ids
	 */
	private static LinkedList<String> createKnownFlightIDList(LinkedList<Passenger> correctList)
	{
		LinkedList<String> knownFlightID = new LinkedList<String>();
		for(int i = 0; i < correctList.size(); i++)
		{
			Passenger tempPassenger = correctList.get(i);
			String flightID = tempPassenger.getFlightID();
			if(!knownFlightID.contains(flightID))
			{
				knownFlightID.add(flightID);
			}
		}
		return knownFlightID;
	}
	
	/*
	 * creates a list of known airport codes
	 */
	private static LinkedList<String> createKnownFlightCode(LinkedList<Airport> airportList)
	{
		LinkedList<String> knownFlightCode = new LinkedList<String>();
		for(int i = 0; i < airportList.size(); i++)
		{
			Airport tempAirport = airportList.get(i);
			String airportCode = tempAirport.getAirportIATAFFA();
			if(!knownFlightCode.contains(airportCode))
			{
				knownFlightCode.add(airportCode);
			}
		}
		
		return knownFlightCode;
	}
		
	/*
	 * creates the airport list
	 */
	private static LinkedList<Airport> createAirportList(String filePath)
	{
		LinkedList<Airport> airportContainer = new LinkedList<Airport>();
		
		BufferedReader br = null;
		
		try
		{
			br = new BufferedReader(new FileReader(filePath));
			
			String line = "";
			
			while((line = br.readLine()) != null)
			{
				String[] airportInformation = line.split(",");
				if(airportInformation.length >= 4)
				{
					Airport airportTemp = new Airport();
					airportTemp.setAirportName(airportInformation[0]);
					airportTemp.setAirportIATAFFA(airportInformation[1]);
					airportTemp.setLatitude(airportInformation[2]);
					airportTemp.setLongitude(airportInformation[3]);
					airportContainer.add(airportTemp);
				}
			}				
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				br.close();
			}
			catch(IOException ioe)
			{
				System.out.println("Error occured while closing the BufferedReader");
                ioe.printStackTrace();
			}
		}
		
		
		return airportContainer;
	}
	
	/*Creates the passenger list*/
	private static LinkedList<Passenger> createPassengerList(String filePath)
	{
		LinkedList<Passenger> passengerList = new LinkedList<Passenger>();		
		BufferedReader bufferedReader = null;
		
		try
		{
			bufferedReader = new BufferedReader(new FileReader(filePath));			
			String line = "";			
			while((line = bufferedReader.readLine()) != null)
			{
				if(line.contains("﻿"))
				{
					line = line.replace("﻿", "");
				}
				String[] passengerInformation = line.split(",");				
				Passenger tempPassenger = new Passenger(passengerInformation[0], passengerInformation[1] ,passengerInformation[2], passengerInformation[3], 
														passengerInformation[4], passengerInformation[5]);
				passengerList.add(tempPassenger);
				
			}				
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				bufferedReader.close();
			}
			catch(IOException ioe)
			{
				System.out.println("Error occured while closing the BufferedReader");
                ioe.printStackTrace();
			}
		}
				
		return passengerList;
	}
	
	/*
	 * checks the regular expressions of a passenger
	 */
	private static boolean checkPassengerRegex(Passenger passenger)
	{
		String regex1 = "[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]";
		String regex2 = "[A-Z]{3}[0-9]{4}[A-Z]";
		String regex3 = "[A-Z]{3}";
		String regex4 = "[0-9]{10}";
		String regex5 = "[0-9]{1,4}";
		
		boolean answer = false;
		
		if(passenger.getPassengerID().matches(regex1) && passenger.getFlightID().matches(regex2) &&
				passenger.getStartIATAFFAcode().matches(regex3) && passenger.getEndIATAFFAcode().matches(regex3) &&
				passenger.getDepartureTime().matches(regex4) && passenger.getTotalTime().matches(regex5))
		{
			answer = true;
		}
		return answer;
	}
	
	
	/*
	 * remove duplicates from a given list
	 */
	private static LinkedList<Passenger> removeDuplicates(Queue<Passenger> duplicateList)
	{
		Set<Passenger> nonDuplicate = new HashSet<>();
		nonDuplicate.addAll(duplicateList);
		return new LinkedList<Passenger>(nonDuplicate);		
	}
	
	
	

}
