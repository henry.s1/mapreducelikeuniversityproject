package uk.ac.reading.username;


public class Passenger
{
	private String passengerID;
	private String flightID;
	private String startIATAFFAcode;
	private String endIATAFFAcode;
	private String departureTime;
	private String totalTime;
	
	Passenger(String pid, String fid, String startCode, String endCode, String depart, String total)
	{
		this.passengerID = pid;
		this.flightID = fid;
		this.startIATAFFAcode = startCode;
		this.endIATAFFAcode = endCode;
		this.departureTime = depart;
		this.totalTime = total;
	}
		
	public boolean equals(Object o)
	{
		if(this == o)
		{
			return true;
		}
		if((o == null) || (o.getClass() != this.getClass()))
		{
			return false;
		}
		
		Passenger passenger = (Passenger) o;
		return (passengerID == passenger.passengerID || (passengerID != null && passengerID.equals(passenger.passengerID))) &&
				(flightID == passenger.flightID || (flightID != null && flightID.equals(passenger.flightID))) &&
				(startIATAFFAcode == passenger.startIATAFFAcode || (startIATAFFAcode != null && startIATAFFAcode.equals(passenger.startIATAFFAcode))) &&
				(endIATAFFAcode == passenger.endIATAFFAcode || (endIATAFFAcode != null && endIATAFFAcode.equals(passenger.endIATAFFAcode))) &&
				(departureTime == passenger.departureTime || (departureTime != null && departureTime.equals(passenger.departureTime))) &&
				(totalTime == passenger.totalTime || (totalTime != null && totalTime.equals(passenger.totalTime)));
		
	}
	
	
	public int hashCode()
	{
		int hash = 7;
		hash = 31 * hash + (null == passengerID ? 0 : passengerID.hashCode());
		hash = 31 * hash + (null == flightID ? 0 : flightID.hashCode());
		hash = 31 * hash + (null == startIATAFFAcode ? 0 : startIATAFFAcode.hashCode());
		hash = 31 * hash + (null == endIATAFFAcode ? 0 : endIATAFFAcode.hashCode());
		hash = 31 * hash + (null == departureTime ? 0 : departureTime.hashCode());
		hash = 31 * hash + (null == totalTime ? 0 : totalTime.hashCode());
        return hash;
	}
	
	
		
	
	
	
	// -------------------------------- getters and setters
	/**
	 * @return the passengerID
	 */
	public String getPassengerID()
	{
		return passengerID;
	}

	/**
	 * @param passengerID the passengerID to set
	 */
	public void setPassengerID(String passengerID)
	{
		this.passengerID = passengerID;
	}

	/**
	 * @return the flightID
	 */
	public String getFlightID()
	{
		return flightID;
	}

	/**
	 * @param flightID the flightID to set
	 */
	public void setFlightID(String flightID)
	{
		this.flightID = flightID;
	}

	/**
	 * @return the startIATAFFAcode
	 */
	public String getStartIATAFFAcode()
	{
		return startIATAFFAcode;
	}

	/**
	 * @param startIATAFFAcode the startIATAFFAcode to set
	 */
	public void setStartIATAFFAcode(String startIATAFFAcode)
	{
		this.startIATAFFAcode = startIATAFFAcode;
	}

	/**
	 * @return the endIATAFFAcode
	 */
	public String getEndIATAFFAcode()
	{
		return endIATAFFAcode;
	}

	/**
	 * @param endIATAFFAcode the endIATAFFAcode to set
	 */
	public void setEndIATAFFAcode(String endIATAFFAcode)
	{
		this.endIATAFFAcode = endIATAFFAcode;
	}

	/**
	 * @return the departureTime
	 */
	public String getDepartureTime()
	{
		return departureTime;
	}

	/**
	 * @param departureTime the departureTime to set
	 */
	public void setDepartureTime(String departureTime)
	{
		this.departureTime = departureTime;
	}

	/**
	 * @return the totalTime
	 */
	public String getTotalTime()
	{
		return totalTime;
	}

	/**
	 * @param totalTime the totalTime to set
	 */
	public void setTotalTime(String totalTime)
	{
		this.totalTime = totalTime;
	}
	
}
