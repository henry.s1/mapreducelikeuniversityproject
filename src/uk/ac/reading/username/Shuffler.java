package uk.ac.reading.username;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;

public class Shuffler extends Thread
{
	BlockingQueue<Tuple> tupleQueue; // gets the shared tuplequeue	
	BlockingQueue<TupleList> tList; // puts it into a tuplelist queue
	LinkedList<TupleList> tupleListContainer = new LinkedList<TupleList>(); // used to hold tuplelist tempporarily
	HashMap<String, LinkedList<Tuple>> tupleMap = new HashMap<String, LinkedList<Tuple>>();
	//get object from queue
	
	/*Constructor for the shuffler taking a shared tuple queue and a shared tupleList Queue*/
	public Shuffler(BlockingQueue<Tuple> tQ, BlockingQueue<TupleList> tL)
	{
		this.tupleQueue = tQ;
		this.tList = tL;
	}
	
	@Override 
	public void run()
	{     		
		while(!tupleQueue.isEmpty()) // loop until tupleQueue is empty
		{
			Tuple tempTuple = tupleQueue.poll(); // takes one tuple from the queue
			String key = tempTuple.getValue(); // sets the value as the tuple value
			if(tupleMap.containsKey(key)) 
			{
				tupleMap.get(key).add(tempTuple);// if the map contains the key then add 
			}
			else
			{
				tupleMap.put(key, new LinkedList<Tuple>()); // key not present in map so create new
				tupleMap.get(key).add(tempTuple);
			}
		}
		
		LinkedList<String> usedKeys = new LinkedList<String>();
		for(String key1 : tupleMap.keySet())
		{
			usedKeys.add(key1); // add all used keys into a list
		}
		for(int i = 0 ; i < usedKeys.size();i++)
		{
			LinkedList<Tuple> tempTupleList = tupleMap.get(usedKeys.get(i));
			TupleList tempTuple = new TupleList(usedKeys.get(i), tempTupleList);
			tupleListContainer.add(tempTuple); // the tuplelist to a container
		}
		tList.addAll(tupleListContainer); // adds all of the list to a shared buffer for use with reducer
	}
}