package uk.ac.reading.username;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.TimeZone;
import java.util.concurrent.BlockingQueue;

public class Reducer extends Thread
{	
	BlockingQueue<TupleList> tList;
	//get object from queue
	String key;
	String subject;
	LinkedList<Airport> airportList;
	String filePath;
	String fileName;
	
	
	/*Constructor for the reducer taking the tuplelist buffer, job and airport list, sets file path to user selected*/
	public Reducer(BlockingQueue<TupleList> tL, String s, LinkedList<Airport> aL, String fP, String fN)
	{
		//this.tupleQueue = tQ;
		this.tList = tL;
		this.subject = s;
		this.airportList = aL;
		this.filePath = fP;
		this.fileName = fN;
		
		
	}
	/*Constructor for the reducer taking the tuplelist buffer, job and airport list sets to default file path if none selected*/
	public Reducer(BlockingQueue<TupleList> tL, String s, LinkedList<Airport> aL)
	{
		//this.tupleQueue = tQ;
		this.tList = tL;
		this.subject = s;
		this.airportList = aL;
		this.filePath = "C:/TestFolder/";
		this.fileName = "test.csv";
		
	}
	
	
	@Override 
	public void run()
	{                                        
		// get one tuple arr from tuple list
		// reduce the list
		// write to array then get another method to write to csv
		if(subject.matches("flightsFromAirport")) // check if the job matches the flights from airport
		{
			LinkedList<String> usedAirports = new LinkedList<String>();
			LinkedList<Tuple> usedAir = new LinkedList<Tuple>();
			
			StringBuilder sb = new StringBuilder(); // creates a string builder for the output
			while(!tList.isEmpty()) 
			{
				
				TupleList tempTupleList = tList.poll(); // takes a tuple list from the list
				LinkedList<Tuple> tupleArr = tempTupleList.getTuple(); // create a container for the tuble list
				LinkedList<String> numFlights = new LinkedList<String>(); // creates a container for the number of flights
				for(int i = 0; i < tupleArr.size(); i++) 
				{					
					Tuple tempTuple = tupleArr.get(i); // takes a tuple from the container
					 
					Passenger tempPassenger = (Passenger) tempTuple.getObject(); // creates a passenger container from the tuple
					
					if(!numFlights.contains(tempPassenger.getFlightID()))
					{
						numFlights.add(tempPassenger.getFlightID()); // checks if  contains the passenger flight then add if yes
					}
				}
				for(int i = 0; i < airportList.size();i++)
				{
					Airport airportTemp = airportList.get(i); // gets the first airport in the list
					if(airportTemp.getAirportIATAFFA().matches(tempTupleList.getName())) // check if code matches one in the airport list
					{
						Tuple tempUsed = new Tuple(airportTemp.getAirportName() , numFlights.size()); // create a new tuple for the airport
						usedAir.add(tempUsed);	// adds to used airport list
						usedAirports.add(airportTemp.getAirportName()); // adds the airport name to a list
						break;
					}
				}
			}
			
			/*adds job specific information to the string builder */
			sb.append("Used Airports\n");
			sb.append("Airport Name");
			sb.append(',');
			sb.append("Number of Flights\n");
			
			
			for(int i = 0; i < usedAir.size(); i++)
			{
				sb.append(usedAir.get(i).getValue());
				sb.append(',');
				sb.append(usedAir.get(i).getCount());
				sb.append("\n");
				
			}
			
			sb.append("Unused Airports \n");
			sb.append("Airport Name");
			sb.append(',');
			sb.append("Number of Flights\n");

			for(int i = 0; i < airportList.size(); i++)
			{
				if(!usedAirports.contains(airportList.get(i).getAirportName()))
				{
					sb.append(airportList.get(i).getAirportName());
					sb.append(',');
					sb.append("0");
					sb.append("\n");
				}
			}
			/*Writes the string from the builder to a csv file*/
			PrintWriter pw;
			try
			{
				File file = new File(filePath + fileName);
				file.getParentFile().mkdir();
				pw = new PrintWriter(file);
				pw.write(sb.toString());
				pw.close();
			} 
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			
			
		}
		else if(subject.matches("listFlights") || subject.matches("listPassengers")) // checks if the job is list the flights or passengers
		{
			StringBuilder sb = new StringBuilder();
			if(subject.matches("listFlights"))
			{
				sb.append("Flight ID");
				sb.append(',');
				sb.append("Number of Passengers\n");
			}
			
			
			while(!tList.isEmpty())
			{
				TupleList tempTupleList = tList.poll(); // gets a tuplelist from the list 
				LinkedList<Tuple> tupleArr = tempTupleList.getTuple(); // gets the first tuple from the list
 
				LinkedList<String> numPassengers = new LinkedList<String>(); // container for the number of passengers
				LinkedList<Passenger> passengerList = new LinkedList<Passenger>(); // container for the passenger list
				
				for(int i = 0; i < tupleArr.size(); i++)
				{
					Tuple tempTuple = tupleArr.get(i); // gets tuple i from the tuple array
					Passenger tempPassenger = (Passenger) tempTuple.getObject(); // sets the passenger from the tuple
					
					if(!numPassengers.contains(tempPassenger.getPassengerID()))
					{
						numPassengers.add(tempPassenger.getPassengerID()); // adds to number of passengers if passengerid not in the list
					}
					if(!passengerList.contains(tempPassenger))
					{
						passengerList.add(tempPassenger); // adds to passenger list if the passenger not in the list
					}
				}
				

				/*adds job specific information to the string buffer */
				if(subject.matches("listFlights"))
				{
					
					sb.append(tempTupleList.getName());
					sb.append(',');
					sb.append(numPassengers.size());
					sb.append("\n");
					
				}
				else
				{
					
					sb.append("FlightID");
					sb.append(',');
					sb.append(tempTupleList.getName());
					sb.append(',');
					sb.append("PassengerID");
					sb.append(',');
					sb.append("From");
					sb.append(',');
					sb.append("To");
					sb.append(',');
					sb.append("Departure Time");
					sb.append(',');
					sb.append("Arrival Time");
					sb.append(',');
					sb.append("Flight Duration");
					sb.append("\n");
					
					for(int i = 0; i < passengerList.size(); i++)
					{
						Passenger tempPassenger = passengerList.poll();
						if(tempPassenger != null)
						{
								sb.append(' ');
								sb.append(',');
								sb.append(' ');
								sb.append(',');
								sb.append(tempPassenger.getPassengerID());
								sb.append(',');
								sb.append(tempPassenger.getStartIATAFFAcode());
								sb.append(',');
								sb.append(tempPassenger.getEndIATAFFAcode());
								sb.append(',');
								sb.append(convertTime(tempPassenger.getDepartureTime()));
								sb.append(',');
								sb.append(getArrival(tempPassenger.getDepartureTime() , tempPassenger.getTotalTime()));
								sb.append(',');
								sb.append(tempPassenger.getTotalTime());
								sb.append("\n");
						}
					}
					sb.append("\n");
				}
				
			}
			/*Writes the string from the builder to a csv file*/
			PrintWriter pw;
			try
			{
				File file = new File(filePath + fileName);
				file.getParentFile().mkdir();
				pw = new PrintWriter(file);
				pw.write(sb.toString());
				pw.close();
			} 
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			
		}
		else
		{
			System.err.println("Invalid subject");
		}
	}
	
	/** Returns the arrival time for a given flight in DD:MM:YYYY HH:mm:ss GMT
	 * @param time  the initial time of the flight
	 * @param total the total duration of the flight
	 */
	private static String getArrival(String time, String total)
	{
		long longTime = Long.valueOf(time);		
		Date date = new Date(longTime*1000L);		
		int minute = Integer.parseInt(total) % 60 ; // gets the total time in minutes
		int hour = Integer.parseInt(total) / 60; // gets the total time in hours		
		Calendar cal = Calendar.getInstance(); 
		cal.setTime(date); // set calendar to the date
		cal.add(Calendar.MINUTE, minute); // adds the hours of the flights to date
		cal.add(Calendar.HOUR_OF_DAY, hour); // adds the minutes of flight to date
		date = cal.getTime(); // set the date as the calender one
		SimpleDateFormat sdf = new SimpleDateFormat("DD:MM:YYYY HH:mm:ss");// specify date format
		sdf.setTimeZone(TimeZone.getTimeZone("GMT")); // specify time zone
		String arrivalTime = sdf.format(date); // convert time from epoch to normal date		
		return arrivalTime;
	}
	
	/** Returns the  time for a given flight in DD:MM:YYYY HH:mm:ss GMT
	 * @param time  the initial time of the flight given in unix epoch
	 */
	private static String convertTime(String time)
	{
		long longTime = Long.valueOf(time); // converts string to long
		Date date = new Date(longTime*1000L); // sets new date
		SimpleDateFormat sdf = new SimpleDateFormat("DD:MM:YYYY HH:mm:ss"); // specify date format
		sdf.setTimeZone(TimeZone.getTimeZone("GMT")); // specify time soze
		 
		String convertedTime = sdf.format(date); // convert time from epoch to normal date
		
		return convertedTime;
	}
	
}