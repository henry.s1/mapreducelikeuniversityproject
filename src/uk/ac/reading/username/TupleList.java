package uk.ac.reading.username;

import java.util.LinkedList;

public class TupleList
{
	private String name;
	private LinkedList<Tuple> tuple;
	
	TupleList(String n, LinkedList<Tuple> t)
	{
		this.name = n;
		this.tuple = t;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the tuple
	 */
	public LinkedList<Tuple> getTuple()
	{
		return tuple;
	}

	/**
	 * @param tuple the tuple to set
	 */
	public void setTuple(LinkedList<Tuple> tuple)
	{
		this.tuple = tuple;
	}
	
	
}
