package uk.ac.reading.username;

public class Tuple
{
	private String value;
	private int count;
	private Object object;
	
	
	Tuple(String v, int c, Object o)
	{
		this.value = v;
		this.count = c;
		this.object = o;
	}
	
	Tuple(String v, int c)
	{
		this.value = v;
		this.count = c;
		this.object = null;
	}
	
	
	/**
	 * @return the value
	 */
	public String getValue()
	{
		return value;
	}


	/**
	 * @param value the value to set
	 */
	public void setValue(String value)
	{
		this.value = value;
	}


	/**
	 * @return the count
	 */
	public int getCount()
	{
		return count;
	}


	/**
	 * @param count the count to set
	 */
	public void setCount(int count)
	{
		this.count = count;
	}


	/**
	 * @return the object
	 */
	public Object getObject()
	{
		return object;
	}


	/**
	 * @param object the object to set
	 */
	public void setObject(Object object)
	{
		this.object = object;
	}
	
}
