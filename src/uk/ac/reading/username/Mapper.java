package uk.ac.reading.username;

import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;

public class Mapper extends Thread
{
	LinkedList<Passenger> passengerQueue; // this is the initial list 
	BlockingQueue<Tuple> tupleArr; // adds to the shared array
	
	// make a tuple so value count and object
	
	String mapSubject; // share between shuffle and reducer
	
	
	/*Constructor for the mapper taking a passenger queue, shared tuple queue and the job as a string*/
	public Mapper( LinkedList<Passenger> pQ, BlockingQueue<Tuple> tQ, String mapS)
	{
		this.passengerQueue = pQ;
		this.tupleArr = tQ;
		this.mapSubject = mapS;
	}
	
	
	//get object from queue
	
	@Override 
	public void run()
	{                                       
		while(!passengerQueue.isEmpty())
		{
			Passenger temp = passengerQueue.poll(); // takes one passenger from the queue
			
			if(mapSubject.equals("flightsFromAirport"))
			{			
				// add to the tuple queue with the key as the flight code and 1 as the count	
				tupleArr.add(new Tuple(temp.getStartIATAFFAcode(), 1, temp)); 			 
			}
			else if(mapSubject.equals("listFlights") || mapSubject.equals("listPassengers"))
			{
				// add to passenger tuple with value FLIGHTID and count 1
				tupleArr.add(new Tuple(temp.getFlightID(), 1, temp));
			}
			else
			{
				System.err.println("Invalid map subject please restart"); // notify user that there is invalid job
			}
		}
	}
}
